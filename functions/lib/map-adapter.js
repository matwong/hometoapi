const googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyDXt2PbQGSFUI_LFlvTDQTzizHV4IcVMA4',
  Promise: Promise
  // key: process.env.GOOGLE_MAPS_KEY
})

const getCityFromResult = (result) => {
  const locality = result.address_components
    .find((component) => component.types[0] === 'locality')
  if (locality == null) return null
  return locality.long_name
}

const getAddressFromResult = (result) => {
  return result.formatted_address
}

function getCoordsFromResult (results) {
  const coordinates = results[0].geometry.location
  return {
    latitude: coordinates.lat,
    longitude: coordinates.lng
  }
}

const MapAdapter = {
  lookupAddress: async (address) => {
    const response = await googleMapsClient.geocode({address: address}).asPromise()
    const results = response.json.results
    if (results.length < 1) return {}

    return {
      ...getCoordsFromResult(results),
      city: getCityFromResult(results[0]),
      address: getAddressFromResult(results[0]),
    }
  },

  lookupCoords: async (coords) => {
    let response = await googleMapsClient.reverseGeocode({latlng: coords}).asPromise()
    const results = response.json.results
    if (results.length < 1) return {}

    return {
      ...coords,
      city: getCityFromResult(results[0]),
      address: getAddressFromResult(results[0]),
    }
  }
}

module.exports = MapAdapter
