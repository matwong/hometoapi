// API Documentation: https://docs.aws.amazon.com/lex/latest/dg/lambda-input-response-format.html#using-lambda-response-format

function plainMessage (content, contentType = 'PlainText') {
  return {contentType, content}
}

class DialogActions {
  static fail (content) {
    return this.close(plainMessage(content), 'Failed')
  }

  static fulfill (content) {
    return this.close(plainMessage(content), 'Fulfilled')
  }

  static close (message, fulfillmentState) {
    return {
      dialogAction: {
        type: 'Close',
        fulfillmentState,
        message
      }
    }
  }

  static delegate (slots) {
    return {
      dialogAction: {
        type: 'Delegate',
        slots
      }
    }
  }
}

module.exports = DialogActions
