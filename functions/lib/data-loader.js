const moment = require('moment')

const mealInfoFile = require('../data/meal.json')
const Meal = require('./meal')

class Eligibility {
  constructor(e) {
    this.eligibility = e
  }
  typePred () {
    const weekday = this.eligibility.weekday
    const time = this.eligibility.time
    const weekDay = (() => {
      if (weekday === undefined || weekday === null) {
        return moment().format('ddd').toLowerCase()
      } else {
        return weekday.toLowerCase()
      }
    })()
    const currTime = (() => {
      if (time === undefined || time === null) {
        return moment()
      } else {
        return strToMoment(time)
      }
    })()
    const type = this.eligibility.type
    if (type === null || type === undefined) {
      return (x) => 
        x.dayOfWeek.map(w => w.toLowerCase()).includes(weekDay) && 
          strToMoment(x.startTime).diff(currTime) < 0 && 
          strToMoment(x.endTime).diff(currTime) > 0
    } else {
      return (x) =>
        x.dayOfWeek.map(w => w.toLowerCase()).includes(weekDay) &&
        x.type.toLowerCase() === type.toLowerCase()
    }
  }
  genderPred () {
    const gender = this.eligibility.gender 
    if (gender === null || gender === undefined) {
      // return the ones with no restrictions
      return (x) => x.gender.toLowerCase() === 'mix'
    } else {
      // return the ones matches the eligibility requirement
      return (x) => x.gender.toLowerCase() === gender.toLowerCase()
    }
  }
  racePred () {
    const race = this.eligibility.race 
    if (race === null || race === undefined) {
      // return the one with no restrictions
      return (x) => x.race.length === 0
    } else {
      // return the one matches the eliigiblity requriement
      return (x) => x.race.includes(race.toLowerCase())
    }
  }
  agePred () {
    if (this.eligibility.age === null || this.eligibility.age === undefined) {
      // return the one with no restrictions
      return (x) => x.age.length === 0
    } else {
      const age = Number(this.eligibility.age)
      // return the one matches the eliigiblity requriement
      return (x) => (Number(x.age[0]) <= age) && (Number(x.age[1]) > age)
    }
  }
  all() {
    // console.log(this.eligibility.race)
    const typePred = this.typePred.bind(this)()
    const genderPred = this.genderPred.bind(this)()
    const racePred = this.racePred.bind(this)()
    const agePred = this.agePred.bind(this)()
    return (x) => typePred(x) && genderPred(x) && racePred(x) && agePred(x)
  }
}

const strToMoment = (s) => {
  return moment().hour(Number(s.split(':')[0])).minute(Number(s.split(':')[1]))
}

module.exports = {
  meals: (eligibility, mealInfo = mealInfoFile) => {
    const pred = new Eligibility(eligibility)
    const concat = (x, y) => x.concat(y)
    const flatMap = (xs, f) => xs.map(f).reduce(concat, [])
    return flatMap(mealInfo, (orgInfo) => Meal.fromJson(orgInfo)).filter(pred.all())
  }
}
