// based on find-meals.js from Amazon Lex charbot
const moment = require('moment')
const DialogActions = require('./lib/dialog-actions')
const Location = require('./lib/location')
const DataLoader = require('./lib/data-loader')
const MealFinder = require('./lib/meal-finder')

function formatMeals (closestMeals) {
  const mealString = closestMeals
    .map((meal) => `${meal.organizationName}`)
    .join('\n')

  return mealString
}

//
// getMealLocation
//
exports.getMealLocation = async function(address, eligibility={}) {
  const location = await Location.fromAddress(address)
  const meals = DataLoader.meals(eligibility)
  const mealFinder = new MealFinder(meals, location)
  const closestMeals = mealFinder.find(1)
  if (closestMeals.length === 0) {
    return {
      text: 'There are no meals available.',
      info: 'na'
    }
  } else {
    const formattedMeals = formatMeals(closestMeals)
    const meal = meals[0]
    const mealString = `The meal closest to you is ${formattedMeals} at ${meal.address},` +
      ` finishing at ${meal.endTime}.` + 
      ` It’s a ${meal.walkTime()} walk from where you are.`
    return {
      text: mealString,
      info: meal
    }
  }
}