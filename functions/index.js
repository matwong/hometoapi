const functions = require('firebase-functions');
const cors = require("cors")
const express = require("express")

const mealSkillHandler = require('./mealSkill')

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// This is the raw blob of JSON from https://www.toronto.ca/app_content/homeless-help-shelters/
// TODO: Pull this from some appropriate datastore, and figure out a way to update the data there automatically daily.
data = require('./data/shelter.json');

// ref:
//  https://us-central1-hometoapi.cloudfunctions.net/api/shelter?lat=43.660158&lon=-79.428661

// Stolen from https://stackoverflow.com/a/48805273
/**
 * Calculates the haversine distance between point A, and B.
 * @param {number[]} latlngA [lat, lng] point A
 * @param {number[]} latlngB [lat, lng] point B
 */
function haversineDistance(latlngA, latlngB) {
  const toRad = x => (x * Math.PI) / 180;
  const R = 6371; // km

  const dLat = toRad(latlngB[1] - latlngA[1]);
  const dLatSin = Math.sin(dLat / 2);
  const dLon = toRad(latlngB[0] - latlngA[0]);
  const dLonSin = Math.sin(dLon / 2);

  const a = (dLatSin * dLatSin) +
            (Math.cos(toRad(latlngA[1])) * Math.cos(toRad(latlngB[1])) * dLonSin * dLonSin);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  let distance = R * c;

  return distance;
}

const getNearestShelter = ({lat, lon}) => {
  const nearest = data.data.reduce((nearestSoFarData, currentShelter) => {
    const distToCurrentShelter = haversineDistance([lat, lon], [currentShelter.latitude, currentShelter.longitude]);
    return distToCurrentShelter < nearestSoFarData.dist?
      {dist: distToCurrentShelter, shelter: currentShelter} : nearestSoFarData;
  }, {dist: Infinity, shelter: {}});
  return nearest;
}

// API
/////////////////////////////////////////////////////////////////////////////////

/* Express with CORS */
const app2 = express()
app2.use(cors({ origin: true }))
app2.get("/meal", async (request, response) => {
  if (typeof request.query.address === 'undefined') {
    response.status(400).send({error: 'Query params "address" must be set.'});
    return;
  }
  const eligibility = {
    type: request.query.type,
    gender: request.query.gender,
    race: request.query.race,
    age: request.query.age,
    weekday: request.query.weekday,
    time: request.query.time,
  }
  const result = await mealSkillHandler.getMealLocation(request.query.address, eligibility)
  response.status(200).send({
    "answerText": result.text,
    "info": result.info
  });
});
app2.get("/shower", (request, response) => {
  response.send("/shower")
});
app2.get("/health", (request, response) => {
  response.send("/health")
});
app2.get("/shelter", (request, response) => {
  // response.send("/shelter!")
  // response.send("Find me a shelter API");
  if (typeof request.query.lat === 'undefined' || typeof request.query.lon === 'undefined') {
    response.status(400).send({error: 'Query params "lat" and "lon" must both be set.'});
    return;
  }
  const lat = parseFloat(request.query.lat);
  const lon = parseFloat(request.query.lon);
  if (isNaN(lat) || isNaN(lon)) {
      response.status(400).send({error: 'Query params "lat" and "lon" must both be floats.'});
    return;
  }
  const result = getNearestShelter({lat: request.query.lat, lon: request.query.lon});
  response.status(200).send(result);
});
app2.get("/shelter/nearest", (request, response) => {
  response.send("just to show break down works! /shelter/nearest")
});

const api = functions.https.onRequest(app2)

exports.api = api;


module.exports = {
  api
} 

// app2.listen(3000, () => console.log('Example app listening on port 3000!'))